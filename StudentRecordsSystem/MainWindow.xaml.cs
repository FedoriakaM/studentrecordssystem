﻿using StudentRecordsSystem.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using StudentRecordsSystem.Serialize;


namespace StudentRecordsSystem {
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            try
            {
                BinaryFormatter frm = new BinaryFormatter();
                SerialCaseCard[] toLoad;
                
                using (FileStream fs = new FileStream("DataBase.dat", FileMode.OpenOrCreate))
                {
                    toLoad = (SerialCaseCard[])frm.Deserialize(fs);
                }

                foreach (SerialCaseCard i in toLoad)
                    Placer.Children.Add(new CaseCard(i, Placer));
            }
            catch { }
        }

        private void ToolBar_MouseDown(object sender, MouseButtonEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        private void MinimizeBtn_Click(object sender, RoutedEventArgs e) {
            this.WindowState = WindowState.Minimized;
        }

        private void MaximizeBtn_Click(object sender, RoutedEventArgs e) {
            if (this.WindowState == WindowState.Maximized)
                this.WindowState = WindowState.Normal;
            else
                this.WindowState = WindowState.Maximized;
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e) {
            BinaryFormatter frm = new BinaryFormatter();
            SerialCaseCard[] toSave = new SerialCaseCard[Placer.Children.Count];
            for (int i = 0; i < toSave.Length; i++)
                toSave[i] = new SerialCaseCard((CaseCard)Placer.Children[i]);
            using (FileStream fs = new FileStream("DataBase.dat", FileMode.OpenOrCreate))
            {
                frm.Serialize(fs, toSave);         
            }
            this.Close();
            Application.Current.Shutdown();
        }
        private void addCase_Click(object sender, RoutedEventArgs e) {
            Placer.Children.Add(new CaseCard("New", "Student", Placer));
        }

        private void deleteCase_Click(object sender, RoutedEventArgs e) {
            if (Placer.Children.Count > 0) {
                Placer.Children.RemoveAt(Placer.Children.Count - 1);
            }
        }
    }
}
