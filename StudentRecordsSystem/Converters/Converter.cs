﻿using StudentRecordsSystem.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using StudentRecordsSystem.Serialize;


namespace StudentRecordsSystem.Converters
{
    public static class Converter
    {
        public static byte[] ToByteArray(Image arg)
        {     
            BitmapEncoder encoder = new PngBitmapEncoder();
            byte[] result = null;
            var bitmapSource = arg.Source as BitmapSource;
            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    result = stream.ToArray();
                }
            }
            return result;
        }
        public static BitmapImage ToImage(byte[] args)
        {
            var image = new BitmapImage();
            using (var mem = new MemoryStream(args))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            return image;
        }
    }
    
}
