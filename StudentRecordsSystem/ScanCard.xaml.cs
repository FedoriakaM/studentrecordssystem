﻿using StudentRecordsSystem.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StudentRecordsSystem.Serialize;

namespace StudentRecordsSystem {
    /// <summary>
    /// Логика взаимодействия для ScanCard.xaml
    /// </summary>
    [Serializable]
    public partial class ScanCard : UserControl {
        public WrapPanel wrapPanel;
        public BitmapImage source;

        public ScanCard(WrapPanel wrap, BitmapImage bitmap) {
            InitializeComponent();
            wrapPanel = wrap;
            Img.Source = bitmap;
            source = bitmap;
        }

        private void ChangeBtn_Click(object sender, RoutedEventArgs e) {
            new ImageView(source).ShowDialog();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e) {
            wrapPanel.Children.Remove(this);
        }

        private void Img_MouseDown(object sender, MouseButtonEventArgs e) {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2) {
                new ImageView(source).ShowDialog();
            }
        }
    }   
}
