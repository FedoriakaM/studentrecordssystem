﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.IO;
using StudentRecordsSystem.Converters;

namespace StudentRecordsSystem.Serialize
{
    [Serializable]
    public class SerialCaseCard
    {
        public SerialDocumentCard[] docsCards;
        public DateTime? BirthDate { get; set; }
        public string Speciality { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte[] ImgData { get; set; }

        public SerialCaseCard(CaseCard card)
        {
            BirthDate = card.BirthDate;
            Speciality = card.Speciality;
            Name = card.NameText.Text;
            Surname = card.SurnameText.Text;
            docsCards = new SerialDocumentCard[card.docsCards.Count];
            for (int i = 0; i < card.docsCards.Count; i++)
                docsCards[i] = new SerialDocumentCard(card.docsCards[i]);
            ImgData = Converter.ToByteArray(card.Img);
        }
    }
}
