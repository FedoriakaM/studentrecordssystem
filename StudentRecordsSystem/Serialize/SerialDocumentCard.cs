﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentRecordsSystem.Converters;

namespace StudentRecordsSystem.Serialize
{
    [Serializable]
    public class SerialDocumentCard
    {
        public byte[][] ScanCardsData;
        public string Info;
        public string Title;
        public string TypeOfDoc;
        public SerialDocumentCard(DocumentCard docCard)
        {
            TypeOfDoc = docCard.TypeText.Text;
            Title = docCard.TitleText.Text;
            Info = docCard.InfoText.Text;
            ScanCardsData = new byte[docCard.ScanCards.Count][];
            for (int i = 0; i < docCard.ScanCards.Count; i++)
                ScanCardsData[i] = Converter.ToByteArray(docCard.ScanCards[i].Img);
        }

    }
}
