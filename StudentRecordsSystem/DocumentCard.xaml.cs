﻿using StudentRecordsSystem.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using StudentRecordsSystem.Serialize;
using StudentRecordsSystem.Converters;

namespace StudentRecordsSystem
{
    /// <summary>
    /// Логика взаимодействия для DocumentCard.xaml
    /// </summary>
    
    public partial class DocumentCard : UserControl
    {
        private DocumentDialog document;
        public WrapPanel wrapPanel;
        public List<ScanCard> ScanCards;
        public DocumentCard(WrapPanel panel)
        {
            InitializeComponent();
            wrapPanel = panel;
            ScanCards = new List<ScanCard>();
        }
        public DocumentCard(SerialDocumentCard docCard, WrapPanel panel)
        {
            InitializeComponent();
            wrapPanel = panel;
            ScanCards = new List<ScanCard>();
            document = new DocumentDialog(TitleText.Text, this);
            foreach (byte[] i in docCard.ScanCardsData)
                ScanCards.Add(new ScanCard(document.ScanImages, Converter.ToImage(i)));
            TitleText.Text = docCard.Title;
            TypeText.Text = docCard.TypeOfDoc;
            InfoText.Text = docCard.Info;


        }
        private void ChangeBtn_Click(object sender, RoutedEventArgs e) {
            document = new DocumentDialog(TitleText.Text, this);
            document.ShowDialog();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e) {
            wrapPanel.Children.Remove(this);
        }
    }
    
}
