﻿using StudentRecordsSystem.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using StudentRecordsSystem.Serialize;
using StudentRecordsSystem.Converters;

namespace StudentRecordsSystem
{
    /// <summary>
    /// Логика взаимодействия для CaseCard.xaml
    /// </summary>
    
    public partial class CaseCard : UserControl
    {
        WrapPanel wrapPanel;
        public List<DocumentCard> docsCards;

        public DateTime? BirthDate { get; set; }
        public string Speciality { get; set; }

        public CaseCard(SerialCaseCard card, WrapPanel panel)
        {
            InitializeComponent();
            NameText.Text = card.Name;
            SurnameText.Text = card.Surname;
            wrapPanel = panel;
            Speciality = card.Speciality;
            BirthDate = card.BirthDate;    
            docsCards = new List<DocumentCard>();
            caseDialog = new CaseDialog(NameText.Text + " " + SurnameText.Text, this);
            foreach (SerialDocumentCard i in card.docsCards)
                docsCards.Add(new DocumentCard(i, caseDialog.DocumentPlacer));
            Img.Source = Converter.ToImage(card.ImgData);
        }

        public CaseCard(string name, string surname, WrapPanel panel)
        {
            InitializeComponent();
            NameText.Text = name;
            SurnameText.Text = surname;
            wrapPanel = panel;
            docsCards = new List<DocumentCard>();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e) {
            wrapPanel.Children.Remove(this);
        }
        public CaseDialog caseDialog;
        private void MoreBtn_Click(object sender, RoutedEventArgs e) {
            caseDialog = new CaseDialog(NameText.Text + " " + SurnameText.Text, this);
            caseDialog.ShowDialog();
        }
    }  
}
