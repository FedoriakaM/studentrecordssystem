﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StudentRecordsSystem.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для CaseDialog.xaml
    /// </summary>
    public partial class CaseDialog : Window
    {
        CaseCard card;
        public CaseDialog(string dialogTitle, CaseCard caseCard)
        {
            InitializeComponent();
            DialogTitle.Text = dialogTitle;
            card = caseCard;
            NameText.Text = card.NameText.Text;
            SurnameText.Text = card.SurnameText.Text;
            SpecialityText.Text = card.Speciality;
            if (card.BirthDate != null)
            {
                BirthDatePicker.SelectedDate = (DateTime)card.BirthDate;
            }
            foreach (DocumentCard i in card.docsCards)
            {
                i.wrapPanel = DocumentPlacer;
                DocumentPlacer.Children.Add(i);
                
            }
            this.ThumbnailImage.Source = card.Img.Source; 
        }

        private void ToolBar_MouseDown(object sender, MouseButtonEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed) 
                this.DragMove();
        }

        private void MinimizeBtn_Click(object sender, RoutedEventArgs e) {
            this.WindowState = WindowState.Minimized;
        }
        private void CloseBtn_Click(object sender, RoutedEventArgs e) {
            DocumentPlacer.Children.Clear();
            this.Close();
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e) {
            card.NameText.Text = NameText.Text;
            card.SurnameText.Text = SurnameText.Text;
            card.Speciality = SpecialityText.Text;
            card.BirthDate = BirthDatePicker.SelectedDate;
            card.docsCards = new List<DocumentCard>();
            for (int i = 0; i < DocumentPlacer.Children.Count; i++)
            {
                try
                {
                    DocumentCard j = (DocumentCard)DocumentPlacer.Children[i];
                    card.docsCards.Add(j);
                }
                catch
                {
                    continue;
                }
            }
            card.Img.Source = this.ThumbnailImage.Source;
            DocumentPlacer.Children.Clear();
            this.Close();
        }

        private void AddDocumentBtn_Click(object sender, RoutedEventArgs e) {
            DocumentPlacer.Children.Add(new DocumentCard(DocumentPlacer));
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e) {
            OpenFileDialog openFile = new OpenFileDialog() { Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.png) | *.jpg; *.jpeg; *.jpe; *.png" };
            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(openFile.FileName);
                bitmap.EndInit();
                ThumbnailImage.Source = bitmap;
            }
        }
    }
}
