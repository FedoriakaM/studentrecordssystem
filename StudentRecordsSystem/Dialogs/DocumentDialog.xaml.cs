﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StudentRecordsSystem.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для DocumentDialog.xaml
    /// </summary>
    public partial class DocumentDialog : Window
    {
        DocumentCard document;
        public DocumentDialog(string title, DocumentCard Document)
        {
            InitializeComponent();
            DialogTitle.Text = title;
            document = Document;

            TitleText.Text = document.TitleText.Text;
            InfoText.Text = document.InfoText.Text;
            if (document.TypeText.Text != string.Empty)
                DocumentTypeBox.SelectedIndex = document.TypeText.Text == "Operative" ? 0 : 1;

            foreach (ScanCard card in document.ScanCards)
            {
                card.wrapPanel = ScanImages;
                ScanImages.Children.Add(card);
            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e) {
            ScanImages.Children.Clear();
            this.Close();
        }

        private void MinimizeBtn_Click(object sender, RoutedEventArgs e) {
            this.WindowState = WindowState.Minimized;
        }

        private void ToolBar_MouseDown(object sender, MouseButtonEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        private void ApplyBtn_Click(object sender, RoutedEventArgs e) {
            document.TitleText.Text = TitleText.Text;
            document.InfoText.Text = InfoText.Text;
            if (DocumentTypeBox.SelectedIndex != -1)
                document.TypeText.Text = DocumentTypeBox.SelectedIndex == 0 ? "Operative" : "Reference";

            document.ScanCards = new List<ScanCard>();
            foreach (ScanCard card in ScanImages.Children.OfType<ScanCard>())
                document.ScanCards.Add(card);
            ScanImages.Children.Clear();
            this.Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e) {
            ScanImages.Children.Clear();
            this.Close();
        }

        private void AddDocBtn_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog openFile = new OpenFileDialog() { Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.png) | *.jpg; *.jpeg; *.jpe; *.png" };
            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(openFile.FileName);
                bitmap.EndInit();
                ScanCard scanCard = new ScanCard(ScanImages, bitmap);
                ScanImages.Children.Add(scanCard);
            }
        }
    }
}